--
-- File generated with SQLiteStudio v3.2.1 on mi�. abr. 8 18:13:19 2020
--
-- Text encoding used: System
--
PRAGMA foreign_keys = off;
BEGIN TRANSACTION;

-- Table: almacen
DROP TABLE IF EXISTS almacen;

CREATE TABLE almacen (
    id          INTEGER       PRIMARY KEY AUTOINCREMENT,
    nombre      VARCHAR (255) NOT NULL,
    descripcion VARCHAR (255),
    telefono    INTEGER,
    ciudad_id   INTEGER       NOT NULL,
    FOREIGN KEY (
        ciudad_id
    )
    REFERENCES ciudad (id) 
);

INSERT INTO almacen (id, nombre, descripcion, telefono, ciudad_id) VALUES (1, 'Central', NULL, NULL, 1);
INSERT INTO almacen (id, nombre, descripcion, telefono, ciudad_id) VALUES (2, 'Av. Murillo', NULL, NULL, 3);
INSERT INTO almacen (id, nombre, descripcion, telefono, ciudad_id) VALUES (3, 'Av. Pirai', NULL, NULL, 1);

-- Table: atributo
DROP TABLE IF EXISTS atributo;

CREATE TABLE atributo (
    id          INTEGER       PRIMARY KEY AUTOINCREMENT,
    nombre      VARCHAR (255) NOT NULL,
    descripcion VARCHAR (255) 
);


-- Table: atributo_producto
DROP TABLE IF EXISTS atributo_producto;

CREATE TABLE atributo_producto (
    id          INTEGER       PRIMARY KEY AUTOINCREMENT,
    atributo_id INTEGER       NOT NULL,
    producto_id INTEGER       NOT NULL,
    valor       VARCHAR (150),
    estado      BOOLEAN,
    FOREIGN KEY (
        atributo_id
    )
    REFERENCES atributo (id),
    FOREIGN KEY (
        producto_id
    )
    REFERENCES producto (id) 
);


-- Table: carrito
DROP TABLE IF EXISTS carrito;

CREATE TABLE carrito (
    id             INTEGER PRIMARY KEY AUTOINCREMENT,
    usuario_id     INTEGER,
    created_at     DATE,
    transaccion_id INTEGER,
    FOREIGN KEY (
        usuario_id
    )
    REFERENCES usuario (id),
    FOREIGN KEY (
        transaccion_id
    )
    REFERENCES transaccion (id) 
);


-- Table: catalogo
DROP TABLE IF EXISTS catalogo;

CREATE TABLE catalogo (
    id          INTEGER       PRIMARY KEY AUTOINCREMENT,
    codigo      VARCHAR (100) NOT NULL,
    nombre      VARCHAR (255) NOT NULL,
    descripcion VARCHAR (255) 
);

INSERT INTO catalogo (id, codigo, nombre, descripcion) VALUES (1, '001', 'Primavera', NULL);
INSERT INTO catalogo (id, codigo, nombre, descripcion) VALUES (2, '002', 'Verano', NULL);

-- Table: catalogo_categoria
DROP TABLE IF EXISTS catalogo_categoria;

CREATE TABLE catalogo_categoria (
    id           INTEGER PRIMARY KEY AUTOINCREMENT,
    catalogo_id  INTEGER NOT NULL,
    categoria_id INTEGER NOT NULL,
    FOREIGN KEY (
        catalogo_id
    )
    REFERENCES catalogo (id),
    FOREIGN KEY (
        categoria_id
    )
    REFERENCES categoria (id) 
);

INSERT INTO catalogo_categoria (id, catalogo_id, categoria_id) VALUES (1, 1, 3);
INSERT INTO catalogo_categoria (id, catalogo_id, categoria_id) VALUES (2, 1, 1);
INSERT INTO catalogo_categoria (id, catalogo_id, categoria_id) VALUES (3, 2, 3);
INSERT INTO catalogo_categoria (id, catalogo_id, categoria_id) VALUES (4, 2, 2);

-- Table: categoria
DROP TABLE IF EXISTS categoria;

CREATE TABLE categoria (
    id          INTEGER       PRIMARY KEY AUTOINCREMENT,
    codigo      VARCHAR (100) NOT NULL,
    nombre      VARCHAR (255) NOT NULL,
    descripcion VARCHAR (255) 
);

INSERT INTO categoria (id, codigo, nombre, descripcion) VALUES (1, '0001', 'Muebles', NULL);
INSERT INTO categoria (id, codigo, nombre, descripcion) VALUES (2, '0003', 'Electrodomesticos', NULL);
INSERT INTO categoria (id, codigo, nombre, descripcion) VALUES (3, '0002', 'Ropa', NULL);

-- Table: ciudad
DROP TABLE IF EXISTS ciudad;

CREATE TABLE ciudad (
    id          INTEGER       PRIMARY KEY AUTOINCREMENT,
    nombre      VARCHAR (255) NOT NULL,
    descripcion VARCHAR (255) 
);

INSERT INTO ciudad (id, nombre, descripcion) VALUES (1, 'Santa Cruz', NULL);
INSERT INTO ciudad (id, nombre, descripcion) VALUES (2, 'La Paz', NULL);
INSERT INTO ciudad (id, nombre, descripcion) VALUES (3, 'Potosi', NULL);

-- Table: consumidor
DROP TABLE IF EXISTS consumidor;

CREATE TABLE consumidor (
    id              INTEGER       PRIMARY KEY AUTOINCREMENT,
    ci              VARCHAR (10)  NOT NULL,
    nombre          VARCHAR (100) NOT NULL,
    apellidos       VARCHAR (200) NOT NULL,
    sexo            CHAR,
    fechaNacimiento DATE,
    celular         INTEGER,
    email           VARCHAR (100),
    direcccion      VARCHAR (200) 
);

INSERT INTO consumidor (id, ci, nombre, apellidos, sexo, fechaNacimiento, celular, email, direcccion) VALUES (1, '1234567', 'marco', 'colque', 'M', '11-11-2020', 72858595, 'marcosd@gmail.com', 'av busch 458');
INSERT INTO consumidor (id, ci, nombre, apellidos, sexo, fechaNacimiento, celular, email, direcccion) VALUES (2, '55445566', 'Alfredo', 'Alarcon', 'M', '03-05-2000', 75854896, 'alfredo@gmail.com', 'Av. Medellones 455');
INSERT INTO consumidor (id, ci, nombre, apellidos, sexo, fechaNacimiento, celular, email, direcccion) VALUES (3, '55445544', 'Fernando', 'Medina', 'M', '03-06-1990', 68854896, 'fernando@gmail.com', 'Av. Busch 458');

-- Table: detalle
DROP TABLE IF EXISTS detalle;

CREATE TABLE detalle (
    id             INTEGER PRIMARY KEY AUTOINCREMENT,
    producto_id    INTEGER NOT NULL,
    transaccion_id INTEGER NOT NULL,
    precio         DOUBLE  NOT NULL,
    cantidad       INTEGER NOT NULL,
    subtotal       DOUBLE  DEFAULT 0
                           NOT NULL,
    FOREIGN KEY (
        producto_id
    )
    REFERENCES producto (id),
    FOREIGN KEY (
        transaccion_id
    )
    REFERENCES transaccion (id) 
);

INSERT INTO detalle (id, producto_id, transaccion_id, precio, cantidad, subtotal) VALUES (1, 1, 1, 1000.0, 2, 2000.0);
INSERT INTO detalle (id, producto_id, transaccion_id, precio, cantidad, subtotal) VALUES (2, 2, 1, 700.0, 1, 700.0);
INSERT INTO detalle (id, producto_id, transaccion_id, precio, cantidad, subtotal) VALUES (3, 4, 3, 300.0, 1, 300.0);

-- Table: detalleplan
DROP TABLE IF EXISTS detalleplan;

CREATE TABLE detalleplan (
    id            INTEGER  PRIMARY KEY AUTOINCREMENT,
    interes       INTEGER,
    cuota_inicial INTEGER,
    plazo         INTEGER,
    couta         INTEGER,
    tiempo        DATETIME,
    plan_id       INTEGER  NOT NULL,
    FOREIGN KEY (
        plan_id
    )
    REFERENCES [plan] (id) 
);


-- Table: empresa
DROP TABLE IF EXISTS empresa;

CREATE TABLE empresa (
    id        INTEGER       PRIMARY KEY AUTOINCREMENT,
    nombre    VARCHAR (100),
    nit       INTEGER,
    telefono  INTEGER,
    movil     INTEGER,
    email     VARCHAR (100),
    direccion VARCHAR (100) 
);

INSERT INTO empresa (id, nombre, nit, telefono, movil, email, direccion) VALUES (1, 'TecnoProfe', 11155488, 72391515, 72390202, 'tecnoprofe.info@gmail.com', 'Av. San Martin 548');

-- Table: foto
DROP TABLE IF EXISTS foto;

CREATE TABLE foto (
    id          INTEGER       PRIMARY KEY AUTOINCREMENT,
    url         VARCHAR (255) NOT NULL,
    descripcion VARCHAR (255),
    producto_id INTEGER       NOT NULL,
    FOREIGN KEY (
        producto_id
    )
    REFERENCES producto (id) 
);

INSERT INTO foto (id, url, descripcion, producto_id) VALUES (1, 'image2.jpg', NULL, 1);
INSERT INTO foto (id, url, descripcion, producto_id) VALUES (2, 'image3.jpg', NULL, 1);
INSERT INTO foto (id, url, descripcion, producto_id) VALUES (3, 'img1.jpg', NULL, 4);
INSERT INTO foto (id, url, descripcion, producto_id) VALUES (4, 'img2.jpg', NULL, 4);
INSERT INTO foto (id, url, descripcion, producto_id) VALUES (5, 'img3.jpg', NULL, 4);
INSERT INTO foto (id, url, descripcion, producto_id) VALUES (6, 'image1.jpg', NULL, 1);
INSERT INTO foto (id, url, descripcion, producto_id) VALUES (7, 'immmg.jpg', NULL, 2);
INSERT INTO foto (id, url, descripcion, producto_id) VALUES (8, 'immmg1.jpg', NULL, 2);

-- Table: permiso
DROP TABLE IF EXISTS permiso;

CREATE TABLE permiso (
    id     INTEGER       PRIMARY KEY AUTOINCREMENT,
    nombre VARCHAR (100) NOT NULL
);


-- Table: permiso_rol
DROP TABLE IF EXISTS permiso_rol;

CREATE TABLE permiso_rol (
    permiso_id INTEGER,
    rol_id     INTEGER,
    PRIMARY KEY (
        permiso_id,
        rol_id
    ),
    FOREIGN KEY (
        permiso_id
    )
    REFERENCES permiso (id),
    FOREIGN KEY (
        rol_id
    )
    REFERENCES rol (id) 
);


-- Table: plan
DROP TABLE IF EXISTS [plan];

CREATE TABLE [plan] (
    id     INTEGER       PRIMARY KEY AUTOINCREMENT,
    nombre VARCHAR (100) NOT NULL
);

INSERT INTO "plan" (id, nombre) VALUES (1, 'Contado');
INSERT INTO "plan" (id, nombre) VALUES (2, 'Credito');

-- Table: producto
DROP TABLE IF EXISTS producto;

CREATE TABLE producto (
    id              INTEGER       PRIMARY KEY AUTOINCREMENT,
    codigo          VARCHAR (100) NOT NULL,
    nombre          VARCHAR (255) NOT NULL,
    descripcion     VARCHAR (255),
    stock           INTEGER       DEFAULT 0
                                  CHECK (stock >= 0),
    price           VARCHAR (100),
    proveedor_id    INTEGER       NOT NULL,
    subcategoria_id INTEGER       NOT NULL,
    almacen_id      INTEGER       NOT NULL,
    FOREIGN KEY (
        proveedor_id
    )
    REFERENCES proveedor (id),
    FOREIGN KEY (
        subcategoria_id
    )
    REFERENCES subcategoria (id),
    FOREIGN KEY (
        almacen_id
    )
    REFERENCES almacen (id) 
);

INSERT INTO producto (id, codigo, nombre, descripcion, stock, price, proveedor_id, subcategoria_id, almacen_id) VALUES (1, '000111', 'Ropero de pared', NULL, 50, '450', 1, 1, 1);
INSERT INTO producto (id, codigo, nombre, descripcion, stock, price, proveedor_id, subcategoria_id, almacen_id) VALUES (2, '000222', 'Planta de ropa Acer', NULL, 15, '550', 3, 2, 1);
INSERT INTO producto (id, codigo, nombre, descripcion, stock, price, proveedor_id, subcategoria_id, almacen_id) VALUES (3, '000333', 'Pantal�n Forever 21', NULL, 100, '250', 2, 4, 3);
INSERT INTO producto (id, codigo, nombre, descripcion, stock, price, proveedor_id, subcategoria_id, almacen_id) VALUES (4, '000444', 'Pantal�n Cat', NULL, 200, '300', 2, 4, 2);
INSERT INTO producto (id, codigo, nombre, descripcion, stock, price, proveedor_id, subcategoria_id, almacen_id) VALUES (5, '000555', 'Pantal�n Penguin', NULL, 30, '310', 2, 4, 3);
INSERT INTO producto (id, codigo, nombre, descripcion, stock, price, proveedor_id, subcategoria_id, almacen_id) VALUES (6, '000666', 'Polera Polo', NULL, 150, '150', 4, 6, 1);
INSERT INTO producto (id, codigo, nombre, descripcion, stock, price, proveedor_id, subcategoria_id, almacen_id) VALUES (7, '000777', 'Zapato Cat Amarillo', NULL, 30, '678', 4, 5, 1);
INSERT INTO producto (id, codigo, nombre, descripcion, stock, price, proveedor_id, subcategoria_id, almacen_id) VALUES (8, '000888', 'Zapatillas Nike', NULL, 805, '750', 4, 5, 1);

-- Table: proveedor
DROP TABLE IF EXISTS proveedor;

CREATE TABLE proveedor (
    id        INTEGER       PRIMARY KEY AUTOINCREMENT,
    nombre    VARCHAR (150) NOT NULL,
    nit       INTEGER       DEFAULT Sin_nombre,
    telefono  INTEGER,
    celular   INTEGER,
    email     VARCHAR (150),
    direccion VARCHAR (255) 
);

INSERT INTO proveedor (id, nombre, nit, telefono, celular, email, direccion) VALUES (1, 'ChinaServ', 898984545, '-', 72548456, NULL, NULL);
INSERT INTO proveedor (id, nombre, nit, telefono, celular, email, direccion) VALUES (2, 'Productserti', NULL, NULL, NULL, NULL, NULL);
INSERT INTO proveedor (id, nombre, nit, telefono, celular, email, direccion) VALUES (3, 'SolutionPanama', NULL, NULL, NULL, NULL, NULL);
INSERT INTO proveedor (id, nombre, nit, telefono, celular, email, direccion) VALUES (4, 'Otro', NULL, NULL, NULL, NULL, NULL);
INSERT INTO proveedor (id, nombre, nit, telefono, celular, email, direccion) VALUES (5, 'Tecnology', NULL, NULL, NULL, NULL, NULL);

-- Table: rol
DROP TABLE IF EXISTS rol;

CREATE TABLE rol (
    id     INTEGER       PRIMARY KEY AUTOINCREMENT,
    nombre VARCHAR (100) NOT NULL
);

INSERT INTO rol (id, nombre) VALUES (1, 'root');
INSERT INTO rol (id, nombre) VALUES (2, 'Admin');
INSERT INTO rol (id, nombre) VALUES (3, 'Secretaria');
INSERT INTO rol (id, nombre) VALUES (4, 'Marketing');
INSERT INTO rol (id, nombre) VALUES (5, 'Contabilidad');
INSERT INTO rol (id, nombre) VALUES (6, 'Ventas');

-- Table: subcategoria
DROP TABLE IF EXISTS subcategoria;

CREATE TABLE subcategoria (
    id           INTEGER       PRIMARY KEY AUTOINCREMENT,
    codigo       VARCHAR (100) NOT NULL,
    nombre       VARCHAR (255) NOT NULL,
    descripcion  VARCHAR (255),
    categoria_id INTEGER       NOT NULL,
    FOREIGN KEY (
        categoria_id
    )
    REFERENCES categoria (id) 
);

INSERT INTO subcategoria (id, codigo, nombre, descripcion, categoria_id) VALUES (1, '00011', 'roperos', NULL, 1);
INSERT INTO subcategoria (id, codigo, nombre, descripcion, categoria_id) VALUES (2, '00022', 'planchas', NULL, 2);
INSERT INTO subcategoria (id, codigo, nombre, descripcion, categoria_id) VALUES (3, '00023', 'secadoras', NULL, 2);
INSERT INTO subcategoria (id, codigo, nombre, descripcion, categoria_id) VALUES (4, '00031', 'pantalones', NULL, 3);
INSERT INTO subcategoria (id, codigo, nombre, descripcion, categoria_id) VALUES (5, '00020', 'Zapaillas', NULL, 3);
INSERT INTO subcategoria (id, codigo, nombre, descripcion, categoria_id) VALUES (6, '00019', 'Poleras', NULL, 3);

-- Table: sucursal
DROP TABLE IF EXISTS sucursal;

CREATE TABLE sucursal (
    id              INTEGER       PRIMARY KEY AUTOINCREMENT,
    ciudad_id       INTEGER       NOT NULL,
    empresa_id      INTEGER       NOT NULL,
    nombre_sucursal VARCHAR (150) NOT NULL,
    telefono        INTEGER,
    email           VARCHAR (100),
    direccion       VARCHAR (150) NOT NULL,
    FOREIGN KEY (
        ciudad_id
    )
    REFERENCES ciudad (id),
    FOREIGN KEY (
        empresa_id
    )
    REFERENCES empresa (id) 
);

INSERT INTO sucursal (id, ciudad_id, empresa_id, nombre_sucursal, telefono, email, direccion) VALUES (1, 1, 1, 'Motacu', 4545215, 'motacu@gmail.com', 'Tercer anilo extero entre san martin 458');
INSERT INTO sucursal (id, ciudad_id, empresa_id, nombre_sucursal, telefono, email, direccion) VALUES (2, 2, 1, 'Sopocachi', 6266565, 'sopocachi@gmail.com', 'Sopocachi 487');
INSERT INTO sucursal (id, ciudad_id, empresa_id, nombre_sucursal, telefono, email, direccion) VALUES (3, 1, 1, 'Busch', 848455, 'busch@gmail.com', 'Av. Busch 558');
INSERT INTO sucursal (id, ciudad_id, empresa_id, nombre_sucursal, telefono, email, direccion) VALUES (4, 3, 1, 'Central3', 72390044, 'centra@gmail.com', 'central');
INSERT INTO sucursal (id, ciudad_id, empresa_id, nombre_sucursal, telefono, email, direccion) VALUES (5, 1, 1, 'Central1', 72390044, 'centra@gmail.com', 'central');
INSERT INTO sucursal (id, ciudad_id, empresa_id, nombre_sucursal, telefono, email, direccion) VALUES (6, 2, 1, 'Central2', 72390044, 'centra@gmail.com', 'central');

-- Table: transaccion
DROP TABLE IF EXISTS transaccion;

CREATE TABLE transaccion (
    id            INTEGER      PRIMARY KEY AUTOINCREMENT,
    total         INTEGER      DEFAULT 0,
    estado        BOOLEAN,
    tipo_venta    VARCHAR (20) NOT NULL,
    consumidor_id INTEGER,
    plan_id       INTEGER,
    fecha         DATE,
    FOREIGN KEY (
        consumidor_id
    )
    REFERENCES consumidor (id),
    FOREIGN KEY (
        plan_id
    )
    REFERENCES [plan] (id) 
);

INSERT INTO transaccion (id, total, estado, tipo_venta, consumidor_id, plan_id, fecha) VALUES (1, 1700, NULL, 'Efectivo', 1, 1, '6-4-2020');
INSERT INTO transaccion (id, total, estado, tipo_venta, consumidor_id, plan_id, fecha) VALUES (2, NULL, NULL, 'Virtual', 1, 2, '5-4-2020');
INSERT INTO transaccion (id, total, estado, tipo_venta, consumidor_id, plan_id, fecha) VALUES (3, NULL, NULL, 'Efectivo', 2, 1, '8-4-2020');

-- Table: usuario
DROP TABLE IF EXISTS usuario;

CREATE TABLE usuario (
    id            INTEGER       PRIMARY KEY AUTOINCREMENT,
    username      VARCHAR (100) NOT NULL,
    password      VARCHAR (150) NOT NULL,
    consumidor_id INTEGER       UNIQUE,
    FOREIGN KEY (
        consumidor_id
    )
    REFERENCES consumidor (id) 
);

INSERT INTO usuario (id, username, password, consumidor_id) VALUES (1, 'marco', '1234567', NULL);
INSERT INTO usuario (id, username, password, consumidor_id) VALUES (2, 'Alfredo', '55445544', NULL);
INSERT INTO usuario (id, username, password, consumidor_id) VALUES (3, 'Fernando', '55445544', NULL);

-- Table: usuario_rol
DROP TABLE IF EXISTS usuario_rol;

CREATE TABLE usuario_rol (
    usuario_id INTEGER NOT NULL,
    rol_id     INTEGER NOT NULL,
    PRIMARY KEY (
        usuario_id,
        rol_id
    ),
    FOREIGN KEY (
        usuario_id
    )
    REFERENCES usuario (id),
    FOREIGN KEY (
        rol_id
    )
    REFERENCES rol (id) 
);


COMMIT TRANSACTION;
PRAGMA foreign_keys = on;
